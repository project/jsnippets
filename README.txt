
About
-----
The JSnippets module can be used to insert code snippets, general text templates
and so on into specified Drupal textareas. Snippets can be related to textareas
in forms, using the element ID of the textarea and the form ID of the form.
These can be obtained by way of the JSnippets section button which should be
visible above all textareas for users with the 'administer jsnippets'
permission. Only users with the 'use jsnippets' permission are allowed access
to the snippets.

This module uses Javascript in combination with XMLHTTP and JSON to retrieve and
insert the snippets. It will not work without Javascript.

Example use cases:
  * Inserting canned (consistent) comments / replies to users.
  * Library of PHP code snippets.
  * Create templates for administrative fields such as site off-line messages
    etc.

Bug reports and other feedback are always welcome.

Links
-----
* JSnippets configuration: admin/structure/jsnippets
* Project URL: http://drupal.org/project/jsnippets

Credits
-------
Author: Zen / |gatsby| / Karthik Kumar : http://drupal.org/user/21209
Partially sponsored by: Chris Meryck.