Drupal.behaviors.jsnippets_admin = function() {
  jsnippets = [];
  if (typeof jsnippets_get_all != 'undefined') {
    var jsnippets = jsnippets_get_all();
  }
  $('.form-textarea').each(function(i) {
    if (typeof jsnippets[$(this).attr('id')] == 'undefined') {
      str = Drupal.t('Add to JSnippets');
      $('<div class="jsnippets-admin"><a title="' + str + '">' + str + '</a></div>')
      .prependTo($(this).parent())
      .click(function () {
        var form = $(this).parents('form');
        var url = '&element_id=' + $('textarea', $(this).parent()).attr('id');
        url += '&name=' + $(this).siblings('label').text().replace(/:*\s*$/, '');
        $('input:hidden', form).map(function() {
          if ($(this).attr('name') == 'form_id') {
            url += '&form_id=' + $(this).attr('value');
            location.href = Drupal.settings.basePath + 'admin/build/jsnippets/section/add' + url;
          }
        });
      });
    }
  });
}
