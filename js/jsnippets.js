(function ($) {

Drupal.behaviors.jsnippets = {
  attach: function() {
    var s = Drupal.settings.jsnippets_array;
    jsnippets_add_selects(s);
  }
};

function jsnippets_add_selects(jsnippets) {
  for (var id in jsnippets) {
    var select = jsnippets_add_select($('#' + id).get(0), jsnippets[id], 'jsnippet-' + id);

    select.change(
      function() {
        if (this.value != 'none') {
          var textarea = $('textarea', $(this).parents('.form-item')).get(0);
          var source = $(this).addClass('throbbing');

          $.getJSON(Drupal.settings.basePath + "jsnippets/retrieve/" + this.value, function(json) {
            if (textarea.value.length > 0) {
              textarea.value += '\n\n' + json[source.get(0).value];
            }
            else {
              textarea.value = json[source.get(0).value];
            }
            textarea.scrollTop = textarea.scrollHeight - textarea.clientHeight;
            source.removeClass('throbbing');
          });
        }
      }
    );
  }
}

function jsnippets_add_select(textarea, jsnippet, jsnippet_id) {
  var div = $('<div class="jsnippet" id="' + jsnippet_id + '"><select><option value="none">' + Drupal.t('&nbsp;&nbsp;&nbsp;&nbsp;Insert a snippet') + '</option></select></div>');
  for (var o in jsnippet) {
    $('<option value="' + o + '">' + jsnippet[o] + '</option>').appendTo($('select', div));
  }

  return $(div).prependTo($(textarea).parents('.form-item')).addClass('autocomplete').children('select');
}

}(jQuery));
