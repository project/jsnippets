<?php

/**
 * @file
 *   JSnippets administration scripts.
 *
 * @author
 *   Karthik Kumar : http://drupal.org/user/21209
 */

/**
 * Display an overview of all sections.
 */
function jsnippets_overview() {
  $sections = _jsnippets_get_sections(TRUE);
  $rows = array();
  foreach ($sections as $section) {
    // l() check_plains the name.
    $rows[] = array(l($section->name, 'admin/structure/jsnippets/section/view/' . $section->section_id), $section->jsnippets, l(t('Edit'), 'admin/structure/jsnippets/section/edit/' . $section->section_id), l(t('Delete'), 'admin/structure/jsnippets/section/delete/' . $section->section_id));
  }
  $header = array(t('Section name'), t('Snippets'), array('data' => t('Operations'), 'colspan' => 2));

  return theme('table', array('header' => $header, 'rows' => $rows));
}

/**
 * Add / edit a section.
 */
function jsnippets_section_edit($form, &$form_state, $section_id = NULL) {
  $section = new stdClass();
  $section->name = $section->form_id = $section->element_id = '';

  if (isset($section_id)) {
    $section = _jsnippets_get_section($section_id);
    $form['section_id'] = array('#type' => 'value', '#value' => $section_id);
  }
  else {
    if (isset($_GET['form_id'])) {
      $section->form_id = $_GET['form_id'];
      $section->element_id = $_GET['element_id'];
      $section->name = $_GET['name'];
    }
  }

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Section name'),
    '#description' => t('A name for this section.'),
    '#default_value' => $section->name,
    '#required' => TRUE
  );
  $form['jsnippets_form_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Form ID'),
    '#description' => t('Denotes the form that snippets are to be associated with. e.g. %example', array('%example' => 'block_admin_configure')),
    '#default_value' => $section->form_id,
    '#required' => TRUE
  );
  $form['element_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Element ID'),
    '#description' => t('Denotes the textarea in the above form where the snippets can be inserted.'),
    '#default_value' => $section->element_id,
    '#required' => TRUE
  );

  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));

  return $form;
}

/**
 * Process a section edit form submission.
 */
function jsnippets_section_edit_submit($form, &$form_state) {
  $update = array();
  if (isset($form_state['values']['section_id'])) {
    $update[] = 'section_id';
    $form_state['redirect'] = 'admin/structure/jsnippets';
  }
  // Fix name clash with form API.
  $form_state['values']['form_id'] = $form_state['values']['jsnippets_form_id'];

  $section = jsnippets_section_add($form_state['values'], $update);

  if (empty($update)) {
    // Redirect to new section.
    $form_state['redirect'] = 'admin/structure/jsnippets/section/view/' . $section->section_id;
  }
}

/**
 * Display form to delete a section and its snippets.
 */
function jsnippets_section_delete($form, $form_state, $section_id = NULL) {
  if (isset($section_id) && is_numeric($section_id)) {
    if ($section = _jsnippets_get_section($section_id)) {

      $form['section_id'] = array('#type' => 'value', '#value' => $section_id);
      $form['name'] = array('#type' => 'value', '#value' => $section->name);

      return confirm_form(
        $form,
        t('Are you sure you want to delete the section %name?', array('%name' => $section->name)),
          'admin/structure/jsnippets',
        t('Any snippets in this section will also be lost. This action cannot be undone.'),
        t('Delete'),
        t('Cancel')
      );
    }
  }

  drupal_not_found();
}

/**
 * Process section delete form.
 */
function jsnippets_section_delete_submit($form, &$form_state) {
  db_delete('jsnippets_sections')
  ->condition('section_id', $form_state['values']['section_id'])
  ->execute();
  db_delete('jsnippets')
  ->condition('section_id', $form_state['values']['section_id'])
  ->execute();

  drupal_set_message(t('Section %name and its snippets have been deleted.', array('%name' => $form_state['values']['name'])));
  watchdog('JSnippets', 'Section %name and its snippets have been deleted.', array('%name' => $form_state['values']['name']));

  $form_state['redirect'] = 'admin/structure/jsnippets';
}

/**
 * Display all jsnippets belonging to a section.
 */
function jsnippets_section_view($section_id) {
  if ($section = _jsnippets_get_section($section_id)) {
    drupal_set_title($section->name);
    // Set breadcrumb to allow navigation back to section.
    drupal_set_breadcrumb(_jsnippets_get_breadcrumb());

    $jsnippets = _jsnippets_get_jsnippets($section_id);
    $rows = array();
    foreach ($jsnippets as $jsnippet) {
      $rows[] = array(
        check_plain($jsnippet->name), l(t('Edit'), 'admin/structure/jsnippets/edit/' . $jsnippet->snippet_id),
        l(t('Delete'), 'admin/structure/jsnippets/delete/' . $jsnippet->snippet_id)
      );
    }
    $rows[] = array(
      array('data' => l(t('Add new snippet'), 'admin/structure/jsnippets/add/' . $section_id),
        'colspan' => 3,
        'class' => array('jsnippets-add')
      )
    );

    $header = array(t('Snippet name'), array('data' => 'Operations', 'colspan' => 2));

    return theme('table', array('header' => $header, 'rows' => $rows));
  }

  return drupal_not_found();
}

/**
 * Add/edit a jsnippet.
 */
function jsnippets_jsnippet_edit($form, $form_state, $id = NULL) {
  $jsnippet = new stdClass();
  $jsnippet->name = $jsnippet->value = '';

  if (arg(3) == 'edit' && isset($id) && is_numeric($id)) {
    if ($jsnippet = _jsnippets_get_jsnippet($id)) {
      $form['snippet_id'] = array('#type' => 'value', '#value' => $id);
      $section_id = $jsnippet->section_id;
    }
    else {
      drupal_not_found();
      return;
    }
  }
  // Set variables based on whether this is an add or edit form.
  $title = isset($section_id) ? t('Edit snippet') : t('Add snippet');
  $section_id = isset($section_id) ? $section_id : $id;
  $section = _jsnippets_get_section($section_id);

  if ($section_id) {
    // Set breadcrumb to allow navigation back to section.
    drupal_set_breadcrumb(_jsnippets_get_breadcrumb(array(l($section->name, 'admin/structure/jsnippets/section/view/' . $section_id))));
  }

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Snippet name'),
    '#description' => t('A name to denote this snippet.'),
    '#default_value' => $jsnippet->name,
    '#required' => TRUE
  );
  $form['section_id'] = array(
    '#type' => 'select',
    '#title' => t('Section'),
    '#description' => t('The section that this snippet belongs to.'),
    '#options' => _jsnippets_get_sections(),
    '#default_value' => $section_id,
    '#required' => TRUE
  );
  $form['value'] = array(
    '#type' => 'textarea',
    '#title' => t('Snippet value'),
    '#description' => t('The data associated with this snippet.'),
    '#default_value' => $jsnippet->value,
    '#rows' => 10,
    '#required' => TRUE
  );

  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));

  return $form;
}

/**
 * Process a jsnippet edit form submission.
 */
function jsnippets_jsnippet_edit_submit($form, &$form_state) {
  $update = array();
  if (isset($form_state['values']['snippet_id'])) {
    $update[] = 'snippet_id';
    $form_state['redirect'] = 'admin/structure/jsnippets/section/view/' . $form_state['values']['section_id'];
  }

  jsnippets_snippet_add($form_state['values'], $update);
}

/**
 * Display form to delete a snippet.
 */
function jsnippets_jsnippet_delete($form, $form_state, $jsnippet_id = NULL) {
  if ($jsnippet = _jsnippets_get_jsnippet($jsnippet_id)) {
    $form['jsnippet_id'] = array('#type' => 'value', '#value' => $jsnippet_id);
    $form['section_id'] = array('#type' => 'value', '#value' => $jsnippet->section_id);
    $form['name'] = array('#type' => 'value', '#value' => $jsnippet->name);

    return confirm_form(
      $form,
      t('Are you sure you want to delete the snippet %name?', array('%name' => $jsnippet->name)),
        'admin/structure/jsnippets/section/view/' . $jsnippet->section_id,
      t('This action cannot be undone.'),
      t('Delete'),
      t('Cancel')
    );
  }

  drupal_not_found();
}

/**
 * Process snippet deletion form.
 */
function jsnippets_jsnippet_delete_submit($form, &$form_state) {
  db_delete('jsnippets')
  ->condition('snippet_id', $form_state['values']['jsnippet_id'])
  ->execute();

  drupal_set_message(t('Snippet %name has been deleted.', array('%name' => $form_state['values']['name'])));
  watchdog('JSnippets', 'Snippet %name has been deleted.', array('%name' => $form_state['values']['name']));

  $form_state['redirect'] = 'admin/structure/jsnippets/section/view/' . $form_state['values']['section_id'];
}

/**
 * Helper function to set a custom breadcrumb.
 *
 * @param Array $crumbs
 *   Custom breadcrumb to add to the JSnippet base.
 * @return Array
 *   Merged custom breadcrumb.
 */
function _jsnippets_get_breadcrumb($crumbs = array()) {
  $base = array(
    l(t('Home'), NULL),
    l(t('Administer'), 'admin'),
    l(t('Site building'), 'admin/structure'),
    l(t('JSnippets'), 'admin/structure/jsnippets'),
  );

  return array_merge($base, $crumbs);
}
